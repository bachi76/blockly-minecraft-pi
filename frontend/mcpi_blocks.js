Blockly.defineBlocksWithJsonArray([
    {
        "type": "turtle_move",
        "message0": "Schildkröte: %1 %2",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "DIR",
                "options": [
                    ["Vorwärts", "forward"],
                    ["Rückwärts", "backward"],
                ]
            },
            {
                "type": "input_value",
                "name": "VALUE",
                "check": "Number"
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "",
        "helpUrl": ""
    },

    {
        "type": "turtle_turn",
        "message0": "Schildkröte drehen: %1",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "VALUE",
                "options": [
                    ["Links", "0"],
                    ["Rechts", "1"],
                    ["Hoch", "2"],
                    ["Runter", "3"],
                    ["Rückwärts", "4"],
                ]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "",
        "helpUrl": ""
    },

    {
        "type": "turtle_turn_left",
        "message0": "Schildkröte: Drehe nach links",
        "args0": [
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "",
        "helpUrl": ""
    },

    {
        "type": "turtle_turn_right",
        "message0": "Schildkröte: Drehe nach rechts",
        "args0": [
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "",
        "helpUrl": ""
    },

    {
        "type": "turtle_turn_back",
        "message0": "Schildkröte: Rückwärts drehen",
        "args0": [
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "",
        "helpUrl": ""
    },

    {
        "type": "turtle_pen_start",
        "message0": "Schildkröte: Nimm Stift %1",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "VALUE",
                "options": [
                    ["Stein", "STONE"],
                    ["Holz", "WOOD"],
                    ["Sandstein", "SANDSTONE"],
                    ["Moosstein", "MOSS_STONE"],
                    ["Sand", "SAND"],
                    ["Kies", "GRAVEL"],
                    ["Glas", "GLASS"],
                    ["Ziegelstein", "BRICK_BLOCK"],
                    ["Eisen", "IRON_BLOCK"],
                    ["Gold", "GOLD_BLOCK"],
                    ["Diamant", "DIAMOND_BLOCK"],
                    ["Schnee", "SNOW"],
                    ["TNT", "TNT"],
                    ["Leuchtstein", "GLOWSTONE_BLOCK"],
                    ["Fakel", "TORCH"],
                    ["Zaun", "FENCE"],
                    ["Treppe", "STAIRS_WOOD"],
                    ["Glasscheibe", "GLASS_PANE"],
                    ["Lava", "LAVA"],
                    ["Obsidian", "OBSIDIAN"],
                    ["Ackerland", "FARMLAND"],
                    ["Melone", "MELON"],
                    ["Kaktus", "CACTUS"],
                    ["Pilz", "MUSHROOM_BROWN"],
                    ["Wasser", "WATER_FLOWING"],
                    ["Luft", "AIR"],
                ]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "",
        "helpUrl": ""
    },

    {
        "type": "turtle_pen_start_free",
        "message0": "Schildkröte: Nimm Stift mit Block Id %1 (Typ %2)",
        "args0": [
            {
                "type": "input_value",
                "name": "VALUE",
                "value": 1,
                "min": 0,
                "check": "Number"
            },
            {
                "type": "input_value",
                "name": "SUBVALUE",
                "value": 0,
                "min": 0,
                "check": "Number"
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "",
        "helpUrl": ""
    },

    {
        "type": "turtle_pen_mode",
        "message0": "Schildkröte: Stift %1",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "VALUE",
                "options": [
                    ["Ein", "down"],
                    ["Aus", "up"],
                ]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 350,
        "tooltip": "",
        "helpUrl": ""
    },

    {
        "type": "turtle_mode",
        "message0": "Schildkröte: %1",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "VALUE",
                "options": [
                    ["Fliegen", "fly"],
                    ["Laufen", "walk"],
                ]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "",
        "helpUrl": ""
    },

    {
        "type": "turtle_speed",
        "message0": "Schildkröte: Geschwindigkeit %1",
        "args0": [
            {
                "type": "input_value",
                "name": "VALUE",
                "value": 6,
                "min": 0,
                "max": 10,
                "check": "Number"
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "1 = langsam, 10 = schnell, 0 = Blitz..",
        "helpUrl": ""
    },

    {
        "type": "turtle_pos",
        "message0": "Schildkröte: %1 X:%2 Y:%3 Z:%4",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "METHOD",
                "options": [
                    ["Setzen auf", "absolute"],
                    ["Verschieben um", "relative"],
                    ["Ab Startpunkt verschieben um", "home"],
                ]
            },
            {
                "type": "input_value",
                "name": "X",
                "value": 0,
                "min": -300,
                "max": 300
            },
            {
                "type": "input_value",
                "name": "Y",
                "value": 0,
                "min": -300,
                "max": 300
            },
            {
                "type": "input_value",
                "name": "Z",
                "value": 0,
                "min": -300,
                "max": 300
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 355,
        "tooltip": "",
        "helpUrl": ""
    },

    {
        "type": "turtle_getpos",
        "message0": "%1.%2",
        "args0": [
            {
                "type": "field_dropdown",
                "name": "ENTITY",
                "options": [
                    ["Spieler", "player"],
                    ["Schildkröte", "turtle"]
                ]
            },
            {
                "type": "field_dropdown",
                "name": "AXIS",
                "options": [
                    ["X", "x"],
                    ["Y", "y"],
                    ["Z", "z"]
                ]
            }
        ],
        "output": "Number",
        "colour": 355,
    },
    {
        "type": "sleep",
        "message0": "%1 Sekunden warten",
        "args0": [
            {
                "type": "input_value",
                "name": "VALUE",
                "value": 1,
                "min": 0,
                "check": "Number"
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 356,
        "tooltip": "1 = langsam, 10 = schnell, 0 = Blitz..",
        "helpUrl": ""
    },


]);


// Generators
Blockly.Python['turtle_pen_start'] = function(block) {
    var code = '';
    switch(block.getFieldValue('VALUE')) {

        case 'TNT':
            var code = 'turtle.penblock(block.TNT.id, 1)\n';
            break;

        default:
            var code = 'turtle.penblock(block.' + block.getFieldValue('VALUE') + '.id)\n';
            break;
    }
    return code;
};

Blockly.Python['turtle_pen_start_free'] = function(block) {
    var code = 'turtle.penblock(' + resolveValue(block, 'VALUE') + ', ' + resolveValue(block, 'SUBVALUE')+')\n';
    return code;
};

Blockly.Python['turtle_pen_mode'] = function(block) {
    var code = 'turtle.pen' + block.getFieldValue('VALUE') + '()\n';
    return code;
};

Blockly.Python['turtle_move'] = function(block) {
    var code = '';
    switch (block.getFieldValue('DIR')) {
        case 'backward':
            code = 'turtle.backward(' + resolveValue(block, 'VALUE') + ')\n';
            break;
        case 'forward':
        default:
            code = 'turtle.forward(' + resolveValue(block, 'VALUE') + ')\n';
            break;
    }

    return code;
};

Blockly.Python['turtle_speed'] = function(block) {
    var code = 'turtle.speed(' + resolveValue(block, 'VALUE') + ')\n';
    return code;
};

Blockly.Python['turtle_pos'] = function(block) {
    var x = resolveValue(block, 'X');
    var y = resolveValue(block, 'Y');
    var z = resolveValue(block, 'Z');
    var code = '';

    switch (block.getFieldValue('METHOD')) {

        case 'relative':
            code =  'tx, ty, tz = turtle.position\n'
            code += 'turtle.setposition(tx + ' + x + ', ty + ' + y + ', tz + ' + z + ')\n';
            break;

        case 'home': // Fixme: If the pen is down and we have an offset we'll leave an unwanted mark..
            code =  'turtle.home()\n';
            code += 'tx, ty, tz = turtle.position\n'
            code += 'turtle.setposition(tx + ' + x + ', ty + ' + y + ', tz + ' + z + ')\n';
            break;

        case 'absolute':
        default:
            code = 'turtle.setposition(' + x + ', ' + y + ', ' + z + ')\n';
            break
    }

    return code;
};

Blockly.Python['turtle_mode'] = function(block) {
    var code = 'turtle.' + block.getFieldValue('VALUE') + '()\n';
    return code;
};

Blockly.Python['turtle_turn'] = function(block) {
    var code = '';
    switch(block.getFieldValue('VALUE')) {
        case '0':
            code = 'turtle.left(90)\n';
            break;
        case '1':
            code = 'turtle.right(90)\n';
            break;
        case '2':
            code = 'turtle.up(90)\n';
            break;
        case '3':
            code = 'turtle.down(90)\n';
            break;
        case '4':
        default:
            code = 'turtle.left(180)\n';
            break;
    }
    return code;
};

Blockly.Python['turtle_getpos'] = function(block) {
    var code = '';
    if (block.getFieldValue('ENTITY') === 'player') {
        code = 'mc.player.getPos().' + block.getFieldValue('AXIS');
    } else {
        code = 'turtle.position.' + block.getFieldValue('AXIS')
    }
    return [code, Blockly.Python.ORDER_FUNCTION_CALL];
};

Blockly.Python['sleep'] = function(block) {
    var code = 'sleep(' + resolveValue(block, 'VALUE') + ')\n';
    return code;
};

// Fetch values from inputs with shadow elements
function resolveValue(block, fieldName) {
    var val;
    if (block.getField(fieldName)) {
        // Internal number.
        val = String(parseInt(block.getFieldValue(fieldName), 10));
    } else {
        // External number.
        val = Blockly.Python.valueToCode(block, fieldName, Blockly.Python.ORDER_NONE) || '0';
    }
    return val;
}



