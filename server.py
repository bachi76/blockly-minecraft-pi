import html
import importlib
import json
import locale
import os
import pathlib
from datetime import datetime
import configparser

from bottle import route, run as run, template, post, request, static_file, HTTPResponse, hook, abort

generated_code = None
module_name = "generated_source_code"
FOLDER_CODEFILES = './data/block-codefiles/'

# To ensure utf-8 is set as default encoding. Or do it system-wide (https://www.cyberciti.biz/faq/how-to-set-locales-i18n-on-a-linux-unix/)
locale.setlocale(locale.LC_ALL, 'de_CH.UTF-8')

@post('/run')
def action_run():
	global generated_code
	postdata = request.body.read().decode('utf-8')
	print(postdata)

	try:
		os.rename(module_name + ".py", module_name + "_bak.py")
		print("Backing up previous file")
	except:
		pass

	code = template('generated_source_code', created=datetime.now(), code=postdata)
	code = html.unescape(code)

	with open(module_name + ".py", "w") as code_file:
		code_file.write(code)
		print("Written new code file")

	# Init MC environment
	try:
		if generated_code:
			importlib.reload(generated_code)
		else:
			generated_code = importlib.import_module(module_name)

	#except ConnectionRefusedError:
	#	print("Error: No connection to Minecraft")
	#	return HTTPResponse(status=500, body="Error: No connection to Minecraft")

	except Exception as e:
		msg = "Error: {}".format(e)
		print(msg)
		return HTTPResponse(status=500, body="msg")

	print("Code sucessfully executed! :-)")
	return 'ok'


@hook('before_request')
def check_access():
	remoteaddr = request.environ.get('REMOTE_ADDR')
	forwarded = request.environ.get('HTTP\_X\_FORWARDED_FOR')

	if len(ip_whitelist) == 0 or (remoteaddr in ip_whitelist) or (forwarded in ip_whitelist):
		return
	else:
		abort(403, 'Nothing here for you.')


# Get all files of the extension folder
def get_codefiles():
	dir = pathlib.Path(FOLDER_CODEFILES)
	return [x for x in dir.glob("*.xml")]


@route('/')
def index():
	data = {
		'codefiles': [n.name.replace('.xml', '') for n in get_codefiles()],
		'selectedfile': request.GET.get('file')
	}

	return template('index', data)


@route('/block-codefiles/<filename:re:[a-zA-Z0-9_-]*>')
def load_block_codefile(filename):
	print('Trying to load block xml from: {}'.format(filename))
	return static_file(filename + '.xml', root=FOLDER_CODEFILES)


@post('/block-codefiles/<filename:re:[a-zA-Z0-9_-]*>')
def save_block_codefile(filename):
	postdata = request.body.read().decode('utf-8')
	print(postdata)
	# code = html.unescape(postdata)
	filepath = './data/block-codefiles/' + filename + '.xml'
	with open(filepath, "w") as code_file:
		code_file.write(postdata)
		print("Written new blockly code file to {}".format(filepath))

	return 'ok'
# TODO: Error handling


# Tool to change spreadshet copied data to readable text (tab to dot)
@route('/textumwandler')
def textumwandler():
	return server_static('tab_to_char.html')


@route('/<filepath:path>')
def server_static(filepath):
	return static_file(filepath, root='./frontend/')


# Load config
config = configparser.ConfigParser()
config.read("config.ini")
ip_whitelist = json.loads(config.get('Server', 'ip_whitelist'))



# Start the webserver
# Use 'localhost' to restrict access from the pi. Use '0.0.0.0 for unrestricted access.
# WARNING: Anyone with access to the frontend can execute arbitrary python code on your pi!
run(host='0.0.0.0', port=8081, debug=False)

