"""
Usage: 'python3 grounded' to prevent the player from flying
"""

from time import sleep

from mcpi import block
from mcpi.minecraft import Minecraft

mc = Minecraft.create()
p = mc.player
mc.postToChat("Flügel entfernt...")

while True:
	ground = p.getTilePos()
	# If the tile under the player is air or water, move the player down one tile.
	if mc.getBlock(ground.x, ground.y, ground.z) in [block.AIR.id, block.WATER.id, block.WATER_STATIONARY.id]:
		pos = p.getPos()
		p.setPos(pos.x, pos.y-1, pos.z)

	sleep(0.25)

