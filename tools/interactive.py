"""
Usage: python3 -i tools/interactive.py to init with this
boilerplate and continue in an interactive shell
"""

from mcpi.minecraft import Minecraft
from mcpi import block
from minecraftstuff import MinecraftTurtle

mc = Minecraft.create()
turtle = MinecraftTurtle(mc, mc.player.getTilePos())
p = mc.player
pos = p.getPos()

print("Player position: {} / {} / {}".format(pos.x, pos.y, pos.z))
print("Available objects: mc, p, turtle, pos")
print("\nGoing interactive now.. \n\n")



