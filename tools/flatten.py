"""
Usage: 'python3 flatten <size>' to flatten a square area around the player
"""
import sys
from mcpi.minecraft import Minecraft
from mcpi import block
from minecraftstuff import MinecraftTurtle
from time import sleep

size = int(sys.argv[1])

mc = Minecraft.create()
mc.postToChat("Planieren einer {}x{} grossen Flaeche...".format(size, size))

p = mc.player.getPos()

mc.setBlocks(p.x - size/2, p.y, p.z - size/2, p.x + size/2, p.y + 40, p.z + size/2, block.AIR.id)





