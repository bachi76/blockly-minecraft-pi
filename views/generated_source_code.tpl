"""
Minecraft Pi Control Script
Created: {{created}}

Generator: Blockly for MCPI
"""

from time import sleep

from mcpi.minecraft import Minecraft
from mcpi import block
from minecraftstuff import MinecraftTurtle

mc = Minecraft.create()
turtle = MinecraftTurtle(mc, mc.player.getTilePos())

# Blockly-generated code start

{{code}}

# Blockly-generated code end
